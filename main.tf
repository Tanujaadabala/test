provider "aws" {
  profile    = "default"
  region     = "ap-south-1"
  access_key = "AKIA2DHUPZDLMSJ2IJGD"
  secret_key = "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"

  version = "~> 2.0"
}
# EC2 instance
resource "aws_instance" "main" {
  ami                    = "ami-0e9182bc6494264a4"
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.dmz.id
  key_name               = "key"
  user_data              = "${file("script.sh")}"
  vpc_security_group_ids = [aws_security_group.base.id]
  private_ip             = var.main_instance_private_ip
}

resource "aws_security_group" "base" {
  name        = "Base SG"

  # Outbound HTTPS
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outbound HTTP
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 21
    to_port     = 21
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  # Allow inbound SSH
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    self        = false
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    self        = false
  }



   ingress {
    from_port   = 21
    to_port     = 21
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    self        = false
  }


  vpc_id = aws_vpc.main.id

}

# Elastic IP
resource "aws_eip" "main_eip" {
  vpc = true
}

resource "aws_eip_association" "main_eip_assoc" {
  instance_id   = aws_instance.main.id
  allocation_id = aws_eip.main_eip.id
}

# VPC
resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
}

# Internet gateway
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

# Public Route Table
resource "aws_route_table" "main_public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }
}

# DMZ
resource "aws_subnet" "dmz" {
  vpc_id            = aws_vpc.main.id
  cidr_block       = "10.0.0.0/16"
  availability_zone = "ap-south-1a"


  
}

# Route Table Assoc for DMZ
resource "aws_route_table_association" "dmz" {
  subnet_id      = aws_subnet.dmz.id
  route_table_id = aws_route_table.main_public.id
}


terraform {
  backend "http" {}
}
