variable "name" {}
variable "region" {}
variable "access_key" {}
variable "secret_key" {}
variable "az" {}
variable "key_name" {}
variable "vpc_cidr_block" {}
variable "dmz_cidr_block" {}
variable "main_instance_private_ip" {}
variable "user_data" {}

